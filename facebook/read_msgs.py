#!/bin/python3

import argparse
import io
import json
import math

TAB_LENGTH = 4
MAX_TABS = 5

parser = argparse.ArgumentParser(
    description="Read messages from the specified inbox contained in a Facebook archive.")

parser.add_argument(
    "inbox",
    help="A json file containing the inbox.",
    type=argparse.FileType('r', encoding="UTF-8"))

args = parser.parse_args()

if __name__ == "__main__":
    # Load the inbox for reading
    inbox = json.load(args.inbox)
    messages = inbox["messages"]

    for i in reversed(messages):
        try:
            i["content"]
        except KeyError:
            continue

        name_length = len(i["sender_name"])
        spacing = math.floor(MAX_TABS - (name_length / TAB_LENGTH))

        print(f"[{i['sender_name']}] {'    '*spacing}: \"{i['content']}\"\n")
