#!/bin/bash

BASE_URL='https://api.clashofclans.com/v1'

print_help()
{
	echo "Usage: `basename $0` [player tag] [what]"
	exit 0
}

warn_nokey()
{
	echo 'A key file must exist in this directory.'
	echo 'A key file must be of the following format: `Authorization: Bearer [TOKEN]`'
	exit 1
}

main()
{
	curl -X GET -H @key "$BASE_URL/players/%23$PLAYERTAG" | jq ".$WHAT"
}

if [ $# -eq 0 ]; then
	print_help
elif [ $1 == '-h' ]; then
	print_help
fi

if [ ! -f key ]; then
	warn_nokey
fi

PLAYERTAG=$1
WHAT=$2

main
