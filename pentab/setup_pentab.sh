#!/bin/sh

STYLUS="HUION H420 Pen stylus"
TABLET="HUION H420 Pad pad"

xsetwacom --set "$STYLUS" MapToOutput eDP
xsetwacom --set "$TABLET" Button 1 "key +ctrl +shift +z"
xsetwacom --set "$TABLET" Button 2 "key +ctrl +z"
xsetwacom --set "$TABLET" Button 3 "key +ctrl"
