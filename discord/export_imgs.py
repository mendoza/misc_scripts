#!/bin/python3

import argparse
import os
import discord
import requests

# Parse arguments
parser = argparse.ArgumentParser(
    description="Export images from the specified Discord channel.")
parser.add_argument(
    "token",
    help="Discord bot token.",
    type=str)
parser.add_argument(
    "channel",
    help="The channel'd ID.",
    type=str)
parser.add_argument(
    "output",
    help="The output directory for exported images.",
    type=str
)
args = parser.parse_args()

# Check if specified directory exists, else make a new one
if not os.path.exists(args.output):
    os.mkdir(args.output)

# Discord.py setup
intents = discord.Intents.default()
client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print(f"== Logged in! ==\n")

    success, failed = 0, 0
    channel = await client.fetch_channel(args.channel)

    print(f"Retrieving images from {channel.name}...\n")
    async for entry in channel.history():
        if len(entry.attachments) <= 0:
            continue

        for attachment in entry.attachments:
            if not attachment.content_type.startswith("image/"):
                continue

            res = requests.get(attachment.url)
            if res.status_code != 200:
                print(f"Invalid file URL for file {attachment.filename}")
                failed += 1
                continue

            with open(f"{args.output}/{success}-{attachment.filename}", "wb") as f:
                f.write(res.content)
                print(f"Image is saved to {os.path.abspath(f.name)}")
                success += 1

    print("\nDone!")
    print(f"Processed {success + failed} images")
    print(f"({success} Success, {failed} Failed)")

    await client.close()


client.run(args.token)
